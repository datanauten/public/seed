#!/bin/bash
sshdir="/home/datenhain/.ssh"
adduser datenhain sudo
sed -ie 's/\%sudo.*/\%sudo    ALL=(ALL:ALL) NOPASSWD:ALL/' /etc/sudoers
mkdir -p /home/datenhain/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZ7zjTVPWACkC/91mhYuY03c/dREHxvalEvFTnnTs+ODAO6CH9aRZcluQkg0BwN3FJltueY7kzM3GuW+zualkZk1jxDxGJq3Jz0PR0+lNpreSQ3JuyLA8Ax0zH4ppLdC8FEb2FKEy38IlilYofTAlC4bGWHIOijuKPFIib2p355Air8EmgmhFN0ChsUdEz02YZmXL/90v4MzKiTdDncaGS24hGsGm8TdJucBGnm85fuAGobmXtlEAProiZ9z5NZImnvJ/tyPm3ZPJ1cFbe1EUOoQr+BIrdVAsrnjaUhd+9IAqNnTc0Dmzb+IboIhRcCN21YZkc8SYEO3lYHrHl2hXr ansible bootstrap" > $sshdir/authorized_keys
chown -R datenhain.users $sshdir
chmod 600 $sshdir/authorized_keys

